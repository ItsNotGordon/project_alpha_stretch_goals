# Generated by Django 4.2.5 on 2023-09-16 22:30

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("songs", "0007_alter_song_soundcloud_embed"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="song",
            name="soundcloud_embed",
        ),
        migrations.AddField(
            model_name="song",
            name="spotify_embed",
            field=models.TextField(max_length=500, null=True),
        ),
    ]
