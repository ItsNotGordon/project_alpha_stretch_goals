# Generated by Django 4.2.5 on 2023-09-16 22:17

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("songs", "0005_alter_song_soundcloud_embed"),
    ]

    operations = [
        migrations.AlterField(
            model_name="song",
            name="soundcloud_embed",
            field=models.URLField(max_length=1200, null=True),
        ),
    ]
