# Generated by Django 4.2.5 on 2023-09-15 00:27

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("songs", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="song",
            name="genre",
            field=models.CharField(
                choices=[
                    ("pop", "Pop"),
                    ("hiphop", "Hip Hop"),
                    ("rnb", "RnB"),
                    ("electronic", "Electronic"),
                    ("rock", "Rock"),
                    ("latin", "Latin"),
                    ("country", "Country"),
                    ("alternative", "Alternative"),
                    ("metal", "Metal"),
                    ("korean", "Korean"),
                    ("classical", "Classical"),
                    ("other", "Other"),
                ],
                default="other",
                max_length=11,
            ),
        ),
        migrations.AddField(
            model_name="song",
            name="is_explicit",
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name="song",
            name="album",
            field=models.CharField(max_length=200),
        ),
        migrations.AlterField(
            model_name="song",
            name="artist",
            field=models.CharField(max_length=400),
        ),
        migrations.AlterField(
            model_name="song",
            name="owner",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="songs",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
    ]
